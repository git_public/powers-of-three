#include <iostream>
#define WORD_WIDTH 12
int main()
{
	std::cout.width(WORD_WIDTH); std::cout << "base10" << std::right;
	std::cout.width(WORD_WIDTH); std::cout << "base16" << std::right << std::endl;
	size_t pow = 1;
	while(pow < 10000000)
	{
		std::cout.width(WORD_WIDTH); std::cout << std::dec << pow << std::right;
		std::cout.width(WORD_WIDTH); std::cout << std::hex << pow << std::right << std::endl;
		pow*=3;
	}
	return 0;
}